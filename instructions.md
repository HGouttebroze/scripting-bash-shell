# Instructions

## Echo / Read

- Script ds `vi`
- commencer par le `shebang` (interpréteur binaire)

```sh
#!/bin/bash

echo “Qui es-ce?”

read name

echo "Salut $name”
```

## Incrémenter

```sh
NUMBER=1
let RESULT="++NUMBER"
echo $RESULT
echo $NUMBER
2
2
```

## Conditions

```sh
MY_VALUE=3;
echo -n "Entrer un nombre : "; read USER_VALUE;

if [ "$USER_VALUE" -gt "$MY_VALUE" ]; then
        echo "OK"
else
        echo "Oh No..."
fi
```

## Pratique

- créer 1 script qui génère 1 page web avec des images & leur miniatures
- cmd :
  `convert`+arg `-thumbnail geometry`
