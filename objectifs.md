# Scripting Bash / Shell

## Ecriture de scripts (exemples pr apprendre et progresser)

- utilisation de VIM comme éditeur (voir pdf `vi_cheat_sheet` et autres aides)

* `vi script-hug.sh`

- shebang : interpréteur du script

```sh
#!/bin/bash
```

- permet d'utiliser le binaire bash pr l'interprétation de ce script
- localisation du binaire, utiliser la cmd `which` :

  `which bash`

* commentaires de descriptions
* affichage des options avec echo

- administrer les droits d'exécution du script pour le rendre exécutable (x)
  `chmod 775 script-hug.sh`
- executer le script, lancer cmd :
  `./fileName` soit `./script-hug.sh`
