#!/bin/bash

# Vérification des paramètres
# S'ils sont absents, on met une valeur par défaut

if [ -z $1 ]
then
        sortie='galerie.html'
else
        sortie=$1
fi

# Préparation des fichiers et dossiers

echo '' > $sortie

if [ ! -e img ]
then
        mkdir img 
fi

# En-tête HTML



echo '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"  "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="fr" >
   <head>
       <title>selection images</title>
       <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
       <style type="text/css">
       a img { border:0; }
       </style>
   </head>
   <body>
      <p>' >>$sortie

for image in `ls *.png *.jpg *.jpeg *.svg 2>/dev/null`
do 
	convert $image -thumbnail '200*200' img/$image





	echo '<a href="'$image'"><img src="img/'$img'" alt="" /> </a> '>> $sortie
done

#footer HTML

echo '</p>
   </body>
</html>' >> $sortie
